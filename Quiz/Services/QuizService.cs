﻿using MongoDB.Driver;
using Quiz.Interfaces;
using Quiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quiz.Services
{
    public class QuizService
    {
        private readonly IMongoCollection<Question> _questions;

        public QuizService(IQuizDatabaseSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var db = client.GetDatabase(settings.DatabaseName);

            _questions = db.GetCollection<Question>(settings.QuizCollectionName);
        }

        public List<Question> Get() =>
            _questions.Find(question => true).ToList();

        public Question Create(Question question)
        {
            _questions.InsertOne(question);
            return question;
        }
        
        public List<Question> GetRandom() =>
            _questions.AsQueryable()
            .AsEnumerable()
            .OrderByDescending(x => Guid.NewGuid())
            .Take(5)
            .ToList();
    }
}
