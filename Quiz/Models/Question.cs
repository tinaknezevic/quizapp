﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Quiz.Models
{
    [BsonIgnoreExtraElements]
    public class Question
    {
       
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
        [BsonElement("question")]
        [JsonProperty("Question")]
        public string QuestionText { get; set; }
       
        [BsonElement("answers")]
        [JsonProperty("Answers")]
        public List<Answer> Answers { get; set; }


    }

    public class Answer
    {
        [BsonElement("text")]
        [JsonProperty("Text")]
        public string AnswerText { get; set; }
        [BsonElement("tOf")]
        [JsonProperty("IsTrue")]
        public BsonBoolean IsTrue { get; set; }
    }

}


