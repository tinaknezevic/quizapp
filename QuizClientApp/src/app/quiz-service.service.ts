import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Question } from '../app/question';

@Injectable({
  providedIn: 'root'
})
export class QuizServiceService {

  Url = 'https://localhost:5001/api/quiz';

  constructor(private http: HttpClient) { }


  GetRandomQuestions(): Observable<Question[]> {
    return this.http.get<Question[]>(this.Url);
  }
}
