import { Component, OnInit, AfterViewInit } from '@angular/core';
import { QuizServiceService } from '../quiz-service.service';
import { Question, Answer} from '../question';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit, AfterViewInit {

  questions: Question[];
  results = new Map();
  question: Question;
  activeQuestion: boolean;
  ind: number;
  numOfQues: number;

  constructor(private quizService: QuizServiceService) {
    this.activeQuestion = null;
   }

  ngOnInit() {
    this.activeQuestion = null;
    this.getQuestions();
  }

  async ngAfterViewInit() {
    this.activeQuestion = null;
    this.questions = await this.getQuestions();
    this.numOfQues = this.questions.length;
    this.setQuestions();
}

  setQuestions(): void {
    this.results.clear();
    this.ind = 1;
    this.question = this.questions.pop();
    this.activeQuestion = true;
  }

  getQuestions(): Promise<any> {
    return this.quizService.GetRandomQuestions().toPromise();

  }
  onAnswer(answer: Answer) {
    this.results.set(this.question, answer);
    this.nextQuestion();
  }


  nextQuestion(): void {
    if (this.questions.length === 0) {
      this.activeQuestion = false;
    } else {
      this.question = this.questions.pop();
      this.ind = this.ind + 1;
    }
  }
}
