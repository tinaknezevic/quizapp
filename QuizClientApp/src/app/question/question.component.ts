import { Component, OnInit, Input, EventEmitter, Output, OnChanges} from '@angular/core';
import { Question, Answer } from '../question';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit, OnChanges {

  @Input() question: Question;
  @Input() index: number;
  @Input() numOfQues: number;
  @Output() answered = new EventEmitter<Answer>();
  answer: Answer;

  constructor(public sharedService: SharedService) { }

  ngOnInit() {
  }

  ngOnChanges(): void {
    this.answer = null;
  }

  next(answer: Answer) {
    this.answered.emit(answer);
  }

}
