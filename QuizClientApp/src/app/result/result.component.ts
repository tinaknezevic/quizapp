import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Question, Answer } from '../question';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css']
})
export class ResultComponent implements OnInit {

  @Input() result: Map<Question, Answer>;

  correctAns: number;
  totalNum: number;
  label = 'Correct answers';
  wrongAns = 'Your answer:';

  constructor(public sharedService: SharedService) {
    this.correctAns = 0;
    this.totalNum = 0;
   }


  ngOnInit() {
    this.totalNum = this.result.size;
    this.result.forEach((value: Answer, key: Question) => {
      if (value.IsTrue === true) {
        this.correctAns += 1;
      }
  });
  }

}
