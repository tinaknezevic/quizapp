import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = '˝To know that we know what we know, and to know that we do not know what we do not know, that is true knowledge.˝';
  auth = 'Nicolaus Copernicus';
  start = 'START';
  insert = 'Add question';
  constructor(public sharedService: SharedService) { }

  ngOnInit() {
  }

}
