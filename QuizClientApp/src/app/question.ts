export class Question {
    id: string;
    Question: string;
    Answers: Answer[];

}

export class Answer {
    Text: string;
    IsTrue: boolean;
}
