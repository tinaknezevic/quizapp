import { Injectable } from '@angular/core';
import {Question } from '../app/question';
import { Router } from '@angular/router';

@Injectable()
export class SharedService {
    constructor(private router: Router) {}


    getClassBasedOnValue (key: boolean) {
        if (key === false) {
          return 'rgba(151, 23, 23, 0.568)';
        } else {
          return 'rgba(11, 155, 88, 0.568)';
        }
      }

      goToPage(pageName: string) {
        this.router.navigate([`${pageName}`]);
      }
}
