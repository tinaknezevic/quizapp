
Web aplikacija Quiz rađena je pomoću .Net Core-a (poslužitelj) i Angulara (klijent), podaci (pitanja) su pohranjena u MongoDB u json formatu.
Kako bi se aplikacija ispravno pokrenula potrebno je:

1. Uvesti podatke u MongoDB 
	
	Baza podataka nalazi se u direktoriju quizdb. (Quiz/quizdb)
	
	ime baze podataka: quiz
	ime kolekcije: quiz
	port: 27017
	
	
	***Mongo Server bi trebao prethodno biti instaliran i pokrenut***
	Potrebno je pozicionirati se u direktorij C:\Program Files\MongoDB\Server\4.0\bin (ili slično) te iz terminala pokrenuti
	komandu:  "mongorestore -h localhost:27017 -d quiz <putanja_do>\Quiz\quizdb"
	
2. Pokrenuti poslužitelj

	Unutar direktorija Quiz/Quiz nalazi se programsko riješenje napisano u .Net Coru kojeg je potrebno pokrenuti na
	portu: 5001 (https://localhost:5001/api/quiz)
	
3. Pokrenuti klijentsku aplikaciju

	Klijentska aplikacija nalazi se u direktoriju Quiz/QuizClientApp, pokretanje pomoću naredbe "ng serve" (localhost:4200)

	